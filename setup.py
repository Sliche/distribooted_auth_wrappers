from distutils.core import setup
setup(
  name='distribooted_auth_wrappers',
  packages = ['distribooted_auth_wrappers'], # this must be the same as the name above
  version = '0.1',
  description = 'distribooted authorization wrapper methods',
  author = 'Borislav Sevcik',
  author_email = 'boriss@ballab.com',
  url = 'https://github.com/', # use the URL to the github repo
  download_url = 'https://github.com/sliche/distribooted_auth_wrappers/archive/0.1.tar.gz', # I'll explain this in a second
  keywords = ['distribooted', 'response', 'handler', 'flask'], # arbitrary keywords
  classifiers = [],
)

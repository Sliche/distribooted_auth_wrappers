from flask import request
import requests
from distribooted_response_handler import response_handler

from pprint import pprint
import json


class Authentication(object):

    AUTHENTICATION_SERVER_ADDRESS = "http://192.168.69.76:4446/api/"

    def __init__(self):
        pass

    def requires_authorization(self, wrapped_function):
        def wrapper(*args, **kwargs):

            print request.headers

            secret_token = request.headers['secret-token'] if 'secret-token' in request.headers else None
            access_token = request.headers['access-token'] if 'access-token' in request.headers else None
            print access_token

            if access_token:

                data = {
                    'access_token': access_token,
                    'action': str(args[0].action)
                }

                resp = requests.get(self.AUTHENTICATION_SERVER_ADDRESS + "users/authorize", json=json.dumps(data))
                action_authorized = resp.json()['action_authorized'] if 'action_authorized' in resp.json() else False

                if action_authorized:
                    return wrapped_function(*args, **kwargs)
                else:
                    return response_handler.unauthorized_request('action not authorized')
            else:
                return response_handler.unauthorized_request('access token not present')

        return wrapper

    def set_access_token(self, wrapped_function):
        def wrapper(*args):

            print "before func to return"
            function_to_return = wrapped_function(*args)
            print "after func to return"

            if function_to_return.status_code == 200:

                print function_to_return.response
                data = function_to_return.response[0]
                print [self.AUTHENTICATION_SERVER_ADDRESS + "users/access-token", json.loads(data)]
                resp = requests.get(self.AUTHENTICATION_SERVER_ADDRESS + "users/access-token", json=json.loads(data))
                print resp.json()
                access_token = resp.json()['access_token'] if 'access_token' in resp.json() else None

                if access_token:
                    function_to_return.headers['access_token'] = access_token
                else:
                    return response_handler.bad_gateway("Access token couldnt be set")

            return function_to_return

        return wrapper

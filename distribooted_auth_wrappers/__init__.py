from distribooted_auth_wrappers import Authentication

requires_auth = Authentication().requires_authorization
set_access_token = Authentication().set_access_token
